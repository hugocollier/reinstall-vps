### !important when creating oc i vm make sure to disable 
**Use in-transit encryption in Boot volume section**

### **Several situations require system reinstallation**

1\. My VPS has a bunch of stuff installed and it’s very messy. I want to restore it.

2\. There is a conflict between the previous stuff on my VPS and the stuff I want to install now. Unable to install

3\. My VPS was hacked into and used as a mining machine, and the machine was running at full capacity.

4\. I want to try other versions of bbr, but ARM will still lose contact at the moment.

5\. Purer and more stable

**The most important thing is that if you delete your Oracle Cloud machine, you may not be able to get a new one! So reinstalling the system is very important!**

## **Things to note when reinstalling the system**

Oracle Cloud ARM or AMD architecture, Ubuntu system

dd to debian or ubuntu in a few minutes

## 1\. Run the following **one-click script** :

```
bash <( wget --no-check-certificate -qO- 'https://gitlab.com/hugocollier/reinstall-vps/-/raw/main/InstallNET.sh' ) -u 20.04 -v 64 -a -firmware -p 12345
```
hidden orign
<!-- bash <( wget --no-check-certificate -qO- 'https://moeclub.org/attachment/LinuxShell/InstallNET.sh' ) -d 11 -v 64 -a -firmware -p 12345-->
**System parameters (can be changed to the system and version you want):**

**\-d 11**   \[7, 8, 9, 10, 11\] Debian

**\-u 20.04** \[14.04, 16.04, 18.04, 20.04\] Ubuntu

**The server root password parameter can be changed to another password you want to set:**

**\-p 12345**

**\--port 24587**  ssh port change

## **2\. Wait for the one-click script to end**

**The VPS will automatically disconnect. After successfully installing the system, wait for about three minutes, the VPS will automatically restart, and the dd reinstallation of the system will be completed.**

## 3\. After dd reinstalls the system, you will have root authority directly. You can directly connect and log in with the root username. **The password is 12345 or something else you set yourself.**

 If you want to change the password corresponding to the root user, you can enter the following command. The password will not be displayed when typing, and it will feel like you have not entered it. Just type it normally! Enter it twice!

```
passwd
```

**If you want to change the password corresponding to the root user, you can enter the following command. The password will not be displayed when typing, and it will feel like you have not entered it. Just type it normally! Enter it twice!**

## 4\. **Run the following command before installing BBR on this system.**

```
apt update -y && apt install -y curl && apt install -y socat && apt install wget -y
```

## 5\. Then run **the BBR PLUS four-in-one script**

```
bash <(curl -Lso- https://git.io/kernel.sh)
```

```
wget -N --no-check-certificate "https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh" && chmod +x tcp. sh && ./tcp. sh  
```
last script archived use above one and check there is also vpn xray script in that author

Tutorial is over!